[![Docker Image Size (tag)](https://img.shields.io/docker/image-size/tennox/linkcheck/latest)](https://hub.docker.com/r/tennox/linkcheck) [![GitLab Last Commit](https://img.shields.io/gitlab/last-commit/txlab%2Fdocker-linkcheck)](https://gitlab.com/txlab/docker-linkcheck/activity) [![original source](https://badgen.net/badge/icon/github?icon=github&label=original%20source)](https://github.com/filiph/linkcheck)

# Linkcheck docker image

This image can be used for to run [filiph's linkcheck](https://github.com/filiph/linkcheck) cli (which is the best I've found).

## Auto-updating gitlab repo

- [renovate bot that updates nightly](https://gitlab.com/txlab/docker-linkcheck/-/merge_requests?scope=all&state=all&author_username=txlab_renovate):
  - git submodule `linkcheck` to latest upstream tag
  - latest **alpine** base
- build pipeline publishes to docker hub (and this repo's registry)

## TL;DReadme

```bash
docker run --rm tennox/linkcheck --help
```

### GitLab CI

```yaml
test:
  stage: test
  image:
    name: tennox/linkcheck
    entrypoint: [""]
  services:
    - name: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
      alias: page
  script:
    - IP="$(getent ahosts page | cut -d' ' -f1 | sort -u)" # workaround for: https://github.com/filiph/linkcheck/issues/104
    - "linkcheck -e http://$IP"
```

**More docs [upstream](https://github.com/filiph/linkcheck)**

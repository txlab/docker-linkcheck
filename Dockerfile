# Adapted from: https://hub.docker.com/_/dart/#image-documentation

# Specify the Dart SDK base image version using dart:<version> (ex: dart:2.12)
FROM dart:stable AS build
WORKDIR /app

# Resolve app dependencies.
COPY linkcheck/pubspec.* ./
RUN dart pub get

# Copy app source code and AOT compile it.
COPY linkcheck/ .
# Ensure packages are still up-to-date if anything has changed
RUN dart pub get --offline
RUN dart compile exe bin/linkcheck.dart -o bin/linkcheck


# Build minimal serving image from AOT-compiled `bin/` and required system
# libraries and configuration files stored in `/runtime/` from the build stage.
FROM alpine:3.21

COPY --from=build /runtime/ /
COPY --from=build /app/bin/linkcheck /app/bin/

RUN ln -s /app/bin/linkcheck /usr/local/bin/
ENTRYPOINT ["/app/bin/linkcheck"]